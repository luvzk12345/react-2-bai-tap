import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import meReducer from "./reducers/meReducer";
import listReducer from "./reducers/listReducer";

const reducer = combineReducers({
  //khai báo reducer con
  meReducer,
  listReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

export default store;
