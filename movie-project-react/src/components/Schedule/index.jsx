import React, { Component } from "react";

class Schedule extends Component {
  render() {
    return (
      <div>
        <table
          style={{ color: "rgb(0 0 0 / 54%)" }}
          className="table text-center"
        >
          <thead>
            <tr>
              <th>Ngày chiếu</th>
              <th>Cụm rạp</th>
              <th>Giá vé</th>
              <th>Thời lượng</th>
            </tr>
          </thead>
          <tbody>
            {this.props.lichChieu &&
              this.props.lichChieu.map((item) => {
                const {
                  ngayChieuGioChieu,
                  maLichChieu,
                  thongTinRap,
                  giaVe,
                  thoiLuong,
                } = item;
                return (
                  <tr key={maLichChieu}>
                    <td>{ngayChieuGioChieu}</td>
                    <td>{thongTinRap.tenCumRap}</td>
                    <td>{giaVe}</td>
                    <td>{thoiLuong} phút</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Schedule;
