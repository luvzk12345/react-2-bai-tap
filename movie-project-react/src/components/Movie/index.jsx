import React, { Component } from "react";
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button,
  withStyles,
} from "@material-ui/core";
import styles from "./style";
import { NavLink } from "react-router-dom";

class Movie extends Component {
  render() {
    const { hinhAnh, maPhim, moTa, tenPhim } = this.props.movie;
    const { img, title } = this.props.classes;
    return (
      <Card>
        <CardActionArea>
          <CardMedia className={img} image={hinhAnh} title={tenPhim} />
          <CardContent>
            <Typography
              className={title}
              gutterBottom
              variant="h5"
              component="h2"
            >
              {tenPhim}
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              {moTa.substr(0, 120) + "..."}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <NavLink style={{textDecoration:"none"}} to={"/details/" + maPhim}>
            <Button size="small" color="primary">
              Xem thêm...
            </Button>
          </NavLink>
        </CardActions>
      </Card>
    );
  }
}

export default withStyles(styles)(Movie);
