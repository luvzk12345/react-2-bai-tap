import { Container, Typography, withStyles } from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import pic from "../../assets/img/ami.png";
import cat from "../../assets/img/cat.gif";
import styles from "./style";

class Profile extends Component {
  render() {
    if (!this.props.profile) {
      const { noti, img, content } = this.props.classes;
      return (
        <div className={noti}>
          <div className={content}>
            <h3>QUÝ ZỊ ƠI, ĐĂNG NHẬP ĐỂ XEM THÔNG TIN NHÉ!</h3>
            <img className={img} src={pic}></img>
          </div>
        </div>
      );
    }

    const { taiKhoan, hoTen, email, maNhom, maLoaiNguoiDung } =
      this.props.profile.content;
    const { profile, info, ciao } = this.props.classes;
    return (
      <div className="container mt-5" className={profile}>
        <div>
          <img src={cat} />
        </div>
        <div className="row" className={info}>
          <div className="col-7">
            <h5 className={ciao}>Xin chào {taiKhoan} !!!!!!</h5>
            <table className="table">
              <tbody>
                <tr>
                  <td>Tài khoản: </td>
                  <td>{taiKhoan}</td>
                </tr>
                <tr>
                  <td>Họ tên</td>
                  <td>{hoTen}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>{email}</td>
                </tr>
                <tr>
                  <td>Mã Nhóm</td>
                  <td>{maNhom}</td>
                </tr>
                <tr>
                  <td>Mã người dùng</td>
                  <td>{maLoaiNguoiDung}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    profile: state.meReducer,
  };
};
export default connect(mapStateToProps)(
  withStyles(styles, { withTheme: true })(Profile)
);
