const styles = (theme) => {
  return {
    noti: {
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      margin: "auto",
      height: "400px",
    },
    content: {
      display: "flex",
      flexDirection: "column",
      marginTop: "300px",
      alignItems: "center",
    },
    img: {
      width: "50%",
      marginTop: "20px",
    },
    profile: {
      display: "flex",
      justifyContent: "center",
      marginTop: "100px",
    },
   
    info: {
        width: "50%"
    },
    ciao:{
        textTransform:"uppercase",
        color:"rgb(152, 136, 86)",
        fontWeight: "bold",
        fontSize: "2rem"
    }
  };
};
export default styles;
