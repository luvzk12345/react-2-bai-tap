import { Button, TextField } from "@material-ui/core";
import React, { Component } from "react";
import { signIn } from "../../store/actions/auth";
import { connect } from "react-redux";


class Signin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formValue: {
        taiKhoan: "",
        matKhau: "",
      },
    };
  }

  handleChange = (event) => {
    this.setState({
      formValue: {
        ...this.state.formValue,
        [event.target.name]: event.target.value,
      },
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(
      signIn(this.state.formValue, () => {
        this.props.history.push("/profile");
      })
    );
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-md" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="text-dark text-center">Đăng Nhập</h3>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={this.handleSubmit}>
                <div style={{ marginBottom: 30 }}>
                  <TextField
                    name="taiKhoan"
                    onChange={this.handleChange}
                    fullWidth
                    label="Tài khoản"
                    variant="outlined"
                  />
                </div>
                <div style={{ marginBottom: 30 }}>
                  <TextField
                    type="password"
                    name="matKhau"
                    onChange={this.handleChange}
                    fullWidth
                    label="Mật khẩu"
                    variant="outlined"
                  />
                </div>

                <div>
                  <Button type="submit" variant="contained" color="primary">
                    Đăng Nhập
                  </Button>
                  <Button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Đóng
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect()(Signin);
